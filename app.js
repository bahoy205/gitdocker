require('dotenv').config();

const express = require('express')
const app = express()
const port = process.env.PORT

app.get('/', (req, res) => {
    res.json({
        "code": 200,
        "version": "0.0.1 nodeJs",
        "ID": 67470,
        "Name": "Penpatch",
        "NickName":"Tar"
    })
})
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})  